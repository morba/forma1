﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Threading.Tasks;

namespace Forma1.UnitTests.IntegrationTest
{
    [TestClass]
    public class WebServerStartupTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public WebServerStartupTest()
        {
            //arrenge
            //cant inject the appsettings.json into server startup :( 
            //i should probably use xUnit (since that targets .net core, while ms unit targets plain old .net)
            _server = new TestServer(new WebHostBuilder().UseStartup<Forma1.MVC.Startup>());
            _client = _server.CreateClient();
        }

        [TestMethod]
        public async Task returnHomePage()
        {
            //act
            var response = await _client.GetAsync("/");
            response.EnsureSuccessStatusCode();
            var responseAsString = await response.Content.ReadAsStringAsync();
            //assert
            Assert.IsTrue(responseAsString.Contains("<h2>Csapatok listja</h2>"));
        }

    }
}
