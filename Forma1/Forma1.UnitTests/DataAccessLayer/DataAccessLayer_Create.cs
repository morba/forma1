﻿using Forma1.DataAccess;
using Forma1.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Forma1.UnitTests.DataAccessLayer
{
    [TestClass]
    public class DataAccessLayer_Create
    {
        private Forma1DbContext _dbContext;
        private IDataAccess _dataAccess;


        [TestInitialize]
        public void setup()
        {
            _dbContext = new Forma1DbContext((new DbContextOptionsBuilder().UseInMemoryDatabase("f1-create")).Options);
            _dbContext.SeedDatabase();
            _dataAccess = new DbDataAccess(_dbContext);
        }

        [TestMethod]
        public async Task create()
        {
            //arrange
            Team team = new Team()
            {
                id = Guid.NewGuid(),
                name = "Foo Bar",
                founded = DateTime.UtcNow,
                championshipsWon = 0,
                registrationFeePaid = false,
            };
            //act
            await _dataAccess.createNewTeam(team);
            var teams = await _dataAccess.listAllTeams();
            //assert
            Assert.IsNotNull(teams);
            Assert.AreEqual(4, teams.Count());
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            var team2 = teams.SingleOrDefault(t => t.id == team.id);
            Assert.IsNotNull(team2);
            Assert.AreEqual(team.name, team2.name);
            Assert.AreEqual(team.championshipsWon, team2.championshipsWon);
            Assert.AreEqual(team.founded, team2.founded);
            Assert.AreEqual(team.registrationFeePaid, team2.registrationFeePaid);
            Assert.AreEqual(team.yearOfFoundation, team2.yearOfFoundation);

            //assert db
            Assert.AreEqual(4, _dbContext.teams.Count());
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            //system memory limitations, or future EF versions might break this, 
            //but it should work fine now. source: "Dude, trust me!"
            Assert.AreSame(team, _dbContext.teams.SingleOrDefault(t => t.id == team.id));
        }
    }

}
