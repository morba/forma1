﻿using Forma1.DataAccess;
using Forma1.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forma1.UnitTests.DataAccessLayer
{
    public static class Forma1DbContextExtensions
    {
        public static void SeedDatabase(this Forma1DbContext db)
        {
            if (!db.teams.Any())
            {
                List<Team> temp = new List<Team>();
                foreach(var t in testTeams)
                {
                    temp.Add(new Team().update(t));
                }
                db.teams.AddRange(temp);
                db.SaveChanges();
            }
        }

        public static readonly Guid ferrariId = Guid.ParseExact("498f576d-06b9-4b1e-bc69-b38735c84302", "D");
        public static readonly Guid mercedesId = Guid.ParseExact("a8579df0-127c-4954-9e6c-7de29cd9a6f1", "D");
        public static readonly Guid kidsId = Guid.ParseExact("f1aa6bae-6cc3-4264-99be-c828bd67815f", "D");

        public static readonly Team[] testTeams = new Team[]
        {
            new Team()
            {
                id = ferrariId,
                championshipsWon = 17,
                founded = new DateTime(1947, 08, 13),
                name = "Ferrari",
                registrationFeePaid = false, //italians...
            },
            new Team()
            {
                id = mercedesId,
                championshipsWon = 5,
                founded = new DateTime(1917, 03, 17),
                name = "Mercedes",
                registrationFeePaid = true, 
            },
            new Team()
            {
                id = kidsId,
                championshipsWon = 0,
                founded = new DateTime(2018, 03, 17),
                name = "New kids on the block",
                registrationFeePaid = false,
            }
        };

        
    }

}
