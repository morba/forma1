﻿using Forma1.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace Forma1.UnitTests.DataAccessLayer
{
    [TestClass]
    public class DataAccessLayer_ReadOnly
    {
        [TestMethod]
        public async Task list()
        {
            //arrange
            var _dbContext = new Forma1DbContext((new DbContextOptionsBuilder().UseInMemoryDatabase("some fuking unique value")).Options);
            _dbContext.SeedDatabase();
            var _dataAccess = new DbDataAccess(_dbContext);
            //act
            var teams = await _dataAccess.listAllTeams();
            //assert
            Assert.IsNotNull(teams);
            Assert.AreEqual(3, teams.Count());
            Assert.AreEqual("Ferrari", teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId).name);
            Assert.AreEqual(false, teams.SingleOrDefault(t => t.name == "Ferrari").registrationFeePaid);
            Assert.AreEqual(5, teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId).championshipsWon);
            Assert.AreEqual(2018, teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId).yearOfFoundation);
        }
    }

}
