﻿using Forma1.DataAccess;
using Forma1.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Forma1.UnitTests.DataAccessLayer
{

    [TestClass]
    public class DataAccessLayer_Modify
    {
        /// <summary>
        /// this use the "more trusted" method to modify a team
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task modify()
        {
            //arrange
            var _dbContext = new Forma1DbContext((new DbContextOptionsBuilder().UseInMemoryDatabase("f1-modify")).Options);
            _dbContext.SeedDatabase();
            var _dataAccess = new DbDataAccess(_dbContext);
            var team = await _dataAccess.getTeamById(Forma1DbContextExtensions.ferrariId);
            team.championshipsWon++;
            team.registrationFeePaid = true;
            //act
            await _dataAccess.modifyTeam(team);
            var teams = await _dataAccess.listAllTeams();
            //assert
            Assert.IsNotNull(teams);
            Assert.AreEqual(3, teams.Count());
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            var team2 = teams.SingleOrDefault(t => t.id == team.id);
            Assert.IsNotNull(team2);
            Assert.AreEqual(team.name, team2.name);
            Assert.AreEqual(team.championshipsWon, team2.championshipsWon);
            Assert.AreEqual(team.founded, team2.founded);
            Assert.AreEqual(team.registrationFeePaid, team2.registrationFeePaid);
            Assert.AreEqual(team.yearOfFoundation, team2.yearOfFoundation);

            //assert db
            Assert.AreEqual(3, _dbContext.teams.Count());
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            //system memory limitations, or future EF versions might break this, 
            //but it should work fine now. source: "Dude, trust me!"
            Assert.AreSame(team, _dbContext.teams.SingleOrDefault(t => t.id == team.id));
        }

        [TestMethod]
        public async Task modify_nonexistent()
        {
            //arrange
            var _dbContext = new Forma1DbContext((new DbContextOptionsBuilder().UseInMemoryDatabase("f1-modify-nonexsistent")).Options);
            _dbContext.SeedDatabase();
            var _dataAccess = new DbDataAccess(_dbContext);
            Team team = new Team()
            {
                id = Guid.NewGuid(),
                name = "Foo Bar",
                founded = DateTime.UtcNow,
                championshipsWon = 0,
                registrationFeePaid = false,
            };
            //act
            var res0 = await _dataAccess.modifyTeam(team);
            Assert.IsFalse(res0);
            var teams = await _dataAccess.listAllTeams();
            //assert
            Assert.IsNotNull(teams);
            Assert.AreEqual(3, teams.Count());
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            Assert.IsNull(teams.SingleOrDefault(t => t.id == team.id));
            //assert db
            Assert.AreEqual(3, _dbContext.teams.Count());
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            Assert.IsNull(_dbContext.teams.SingleOrDefault(t => t.id == team.id));
        }

        /// <summary>
        /// a different method to modify a team
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task update()
        {
            //arrange
            var _dbContext = new Forma1DbContext((new DbContextOptionsBuilder().UseInMemoryDatabase("f1-update")).Options);
            _dbContext.SeedDatabase();
            var _dataAccess = new DbDataAccess(_dbContext);
            var team = await _dataAccess.getTeamById(Forma1DbContextExtensions.ferrariId);
            team.championshipsWon++;
            team.registrationFeePaid = true;
            //act
            await _dataAccess.editTeam(team);
            var teams = await _dataAccess.listAllTeams();
            //assert
            Assert.IsNotNull(teams);
            Assert.AreEqual(3, teams.Count());
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            var team2 = teams.SingleOrDefault(t => t.id == team.id);
            Assert.IsNotNull(team2);
            Assert.AreEqual(team.name, team2.name);
            Assert.AreEqual(team.championshipsWon, team2.championshipsWon);
            Assert.AreEqual(team.founded, team2.founded);
            Assert.AreEqual(team.registrationFeePaid, team2.registrationFeePaid);
            Assert.AreEqual(team.yearOfFoundation, team2.yearOfFoundation);

            //assert db
            Assert.AreEqual(3, _dbContext.teams.Count());
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            //system memory limitations, or future EF versions might break this, 
            //but it should work fine now. source: "Dude, trust me!"
            Assert.AreSame(team, _dbContext.teams.SingleOrDefault(t => t.id == team.id));
        }
    }

}
