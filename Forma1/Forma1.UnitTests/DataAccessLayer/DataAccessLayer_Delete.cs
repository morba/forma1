﻿using Forma1.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Forma1.UnitTests.DataAccessLayer
{
    [TestClass]
    public class DataAccessLayer_Delete
    {
        [TestMethod]
        public async Task delete()
        {
            //arrange
            var _dbContext = new Forma1DbContext((new DbContextOptionsBuilder().UseInMemoryDatabase("f1-delete")).Options);
            _dbContext.SeedDatabase();
            var _dataAccess = new DbDataAccess(_dbContext);
            //act
            var res0 = await _dataAccess.deleteTeam(Forma1DbContextExtensions.mercedesId);
            Assert.IsTrue(res0);
            var teams = await _dataAccess.listAllTeams();
            //assert
            Assert.IsNotNull(teams);
            Assert.AreEqual(2, teams.Count());
            Assert.IsNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            //assert db
            Assert.AreEqual(2, _dbContext.teams.Count());
            Assert.IsNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
        }

        [TestMethod]
        public async Task delete_nonexistent()
        {
            //arrange
            var _dbContext = new Forma1DbContext((new DbContextOptionsBuilder().UseInMemoryDatabase("f1-delete-nonexsistent")).Options);
            _dbContext.SeedDatabase();
            var _dataAccess = new DbDataAccess(_dbContext);
            //act
            var res0 = await _dataAccess.deleteTeam(Guid.Empty);
            Assert.IsFalse(res0);
            var teams = await _dataAccess.listAllTeams();
            //assert
            Assert.IsNotNull(teams);
            Assert.AreEqual(3, teams.Count());
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
            //assert db
            Assert.AreEqual(3, _dbContext.teams.Count());
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.mercedesId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.ferrariId));
            Assert.IsNotNull(_dbContext.teams.SingleOrDefault(t => t.id == Forma1DbContextExtensions.kidsId));
        }

    }

}
