using Microsoft.VisualStudio.TestTools.UnitTesting;
using Forma1.Models;
using System;

namespace Forma1.UnitTests.ModelValidations
{
    [TestClass]
    public class Test_DateInThePast
    {
        private DateInThePast _testObj;
        private Team _testModel;

        [TestInitialize]
        public void setup()
        {
            _testModel = new Team();
            _testObj = new DateInThePast();
        }

        [DataTestMethod]
        [DataRow(1996, 08, 31, 0, 0, 0, true, DateTimeKind.Utc)]
        [DataRow(2011, 12, 12, 0, 0, 0, true, DateTimeKind.Local)]
        [DataRow(1968, 4, 8, 5, 5, 5, true, DateTimeKind.Unspecified)]
        [DataRow(2111, 12, 12, 0, 0, 0, false, DateTimeKind.Utc)]
        [DataRow(1, 1, 1, 0, 0, 0, true, DateTimeKind.Utc)]
        [DataRow(69, 6, 9, 0, 0, 0, true, DateTimeKind.Unspecified)]
        [DataRow(666, 6, 16, 0, 0, 0, true, DateTimeKind.Local)]
        [DataRow(3333, 12, 12, 9, 15, 33, false, DateTimeKind.Unspecified)]
        [DataRow(2020, 02, 02, 22, 22, 22, false, DateTimeKind.Local)]
        [TestMethod]
        public void basic(int year, int month, int day, int hours, int minutes, int seconds, bool ok, DateTimeKind kind)
        {
            //arrange
            _testModel.founded = new DateTime(year, month, day, hours, minutes, seconds, kind);
            _testObj = new DateInThePast(kind);
            //act
            bool res0 = _testObj.Match(_testModel.founded);
            bool res1 = _testObj.IsValid(_testModel.founded);
            //assert
            Assert.IsTrue(res0);
            Assert.AreEqual(ok, res1);
        }

        [DataTestMethod]
        [DataRow(4)]
        [DataRow("kfretwiohjnoilsewf")]
        [DataRow(false)]
        [DataRow(true)]
        [DataRow("2011.01.01.")]
        [DataRow("2011-01-01")]
        [DataRow("01-01-01")]
        [DataRow("01/01/01")]
        [DataRow("01.01.01.")]
        [TestMethod]
        public void failType(object o)
        {
            Assert.IsFalse(_testObj.Match(o));
            Assert.IsFalse(_testObj.IsValid(o));
        }

        [DataTestMethod]
        //works only if your system local is hungarian...
        [DataRow("hu-HU", -5, false)]
        //apparently this does not work on windows. thread culture is not respected when fetching the local time...
        //i am not gonna set up VMs / docker images with different locals just to test this...
        //[DataRow("ja-JP", -5, false)]
        //[DataRow("en-US", 5, true)]
        //[DataRow("pt-BR", 5, true)]
        [TestMethod]
        public void timeZones(string timezone, int addMinutes, bool shouldSucceed)
        {
            //arrange
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(timezone);
            //act
            var res = (new DateInThePast(DateTimeKind.Utc)).IsValid(DateTime.Now.AddMinutes(addMinutes));
            //assert
            Assert.AreEqual(shouldSucceed, res);
        }
    }
}
