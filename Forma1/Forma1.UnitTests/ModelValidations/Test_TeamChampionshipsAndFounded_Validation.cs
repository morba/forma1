﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Forma1.Models;
using System;

namespace Forma1.UnitTests.ModelValidations
{
    [TestClass]
    public class Test_TeamChampionshipsAndFounded_Validation
    {
        private Team _testModel;
        private TeamChampionshipsAndFounded_Validation _testObj;

        [TestInitialize]
        public void setup()
        {
            _testModel = new Team();
            _testObj = new TeamChampionshipsAndFounded_Validation("Bazinga!!");
        }

        [DataTestMethod]
        [DataRow(1999,30,false)]
        [DataRow(1999, 3, true)]
        [DataRow(2015, 1, true)]
        [DataRow(2015, 15, false)]
        [TestMethod]
        public void basic(int year, int championsShipsWon, bool expectedResults)
        {
            //arrange
            _testModel.championshipsWon = championsShipsWon;
            _testModel.founded = new DateTime(year, 6, 6);
            //act
            bool res0 = _testObj.Match(_testModel);
            bool res1 = _testObj.IsValid(_testModel);
            //assert
            Assert.IsTrue(res0);
            Assert.AreEqual(expectedResults, res1);
        }

        [DataTestMethod]
        [DataRow(4)]
        [DataRow("kfretwiohjnoilsewf")]
        [DataRow(false)]
        [DataRow(true)]
        [DataRow("2011.01.01.")]
        [DataRow("2011-01-01")]
        [DataRow("01-01-01")]
        [DataRow("01/01/01")]
        [DataRow("01.01.01.")]
        [TestMethod]
        public void failType(object o)
        {
            Assert.IsFalse(_testObj.Match(o));
            Assert.IsFalse(_testObj.IsValid(o));
        }

    }
}
