﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Forma1.API.Controllers;
using Forma1.DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Extensions.Primitives;
using Swashbuckle.AspNetCore.Swagger;

namespace Forma1.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();
            services.AddDbContext<Forma1DbContext>(builder =>
                builder.UseNpgsql(Configuration.GetConnectionString("main")));
            services.AddScoped<IDataAccess>(serv => new DbDataAccess(serv.GetRequiredService<Forma1DbContext>()));

            services.AddMvcCore()
                .AddAuthorization()
                .AddVersionedApiExplorer(options => {
                    options.DefaultApiVersion = new ApiVersion(1, 0);
                    options.GroupNameFormat = "'v'VVV";
                })
                .AddJsonFormatters()
                .AddDataAnnotations()
                .AddCors()
                ;

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = "http://localhost:5000";
                    options.RequireHttpsMetadata = false;

                    options.ApiName = "api_react";
                });

            services.AddApiVersioning(options =>
            {
                //settings
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
                //central source of truth on api versions
                options.Conventions.Controller<TeamController>().HasApiVersion(new ApiVersion(1, 0));
            });

            services.AddSwaggerGen(options => {
                IApiVersionDescriptionProvider provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();

                foreach (var description in provider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(description.GroupName,
                        new Info
                        {
                            Title = $"Forma 1 API v{description.ApiVersion}",
                            Version = description.ApiVersion.ToString(),
                            Description = "Forma 1 React SPA API leíras"
                        });
                }
                options.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Forma1.API.xml"));

            });

            //Add MVC core should have taken care of this
            services.AddCors();
            services.AddMvc();
        }

        //proxy headers for nginx
        private const string XForwardedPath = "X-Forwarded-Path";
        private const string XForwardedPathBase = "X-Forwarded-PathBase";
        private const string XForwardedProto = "X-Forwarded-Proto";

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IApiVersionDescriptionProvider versionProvider)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders =
                  Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor
                | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto
                | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedHost
            });


            //add data manually from proxy headers
            app.Use((context, next) =>
            {
                if (context.Request.Headers.TryGetValue(XForwardedPath, out StringValues path))
                {
                    context.Request.Path = new PathString(path);
                    //Console.WriteLine($"path={path}");
                }

                if (context.Request.Headers.TryGetValue(XForwardedPathBase, out StringValues pathBase))
                {
                    context.Request.PathBase = new PathString(pathBase);
                    //Console.WriteLine($"PathBase={pathBase}");
                }

                if (context.Request.Headers.TryGetValue(XForwardedProto, out StringValues proto))
                {
                    context.Request.Protocol = proto;
                    //Console.WriteLine($"proto={proto}");
                }
                return next();
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                loggerFactory.AddDebug();
            }

            //this is a public facing API
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
                builder.AllowAnyHeader();
            });

            app.UseAuthentication();

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                foreach (var description in versionProvider.ApiVersionDescriptions)
                {
                    options.SwaggerEndpoint($"{description.GroupName}/swagger.json", description.GroupName);
                }
                options.EnableFilter();
            });

            app.UseMvc();
        }
    }
}
