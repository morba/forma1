﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forma1.DataAccess;
using Forma1.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Forma1.API.Controllers
{
    /// <summary>
    /// endpoint for managing teams
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class TeamController : ControllerBase
    {
        private IDataAccess _dataAccess;
        private ILogger<TeamController> _logger;

        /// <summary>
        /// default ctor with DI
        /// </summary>
        /// <param name="dataAccess"></param>
        /// <param name="logger"></param>
        public TeamController(IDataAccess dataAccess, ILogger<TeamController> logger)
        {
            _logger = logger;
            _dataAccess = dataAccess;
        }

        // GET api/values
        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<Team>> get() => await _dataAccess.listAllTeams();

        /// <summary>
        /// get a <see cref="Team"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<Team> get(Guid id) => await _dataAccess.getTeamById(id);

        /// <summary>
        /// create a new <see cref="Team"/>
        /// </summary>
        /// <param name="value">the new team</param>
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> post([FromBody]Team value)
        {
            if (ModelState.IsValid)
            {
                value.id = Guid.NewGuid();
                await _dataAccess.createNewTeam(value);
                return Ok(value.id);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// modify an existing <see cref="Team"/>
        /// </summary>
        /// <param name="id">id of the team to modify</param>
        /// <param name="value">the modified team</param>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> put(Guid id, [FromBody]Team value) =>
            ModelState.IsValid
            ? (await _dataAccess.modifyTeam(value))
                ? (IActionResult)Ok()
                : NotFound()
            : BadRequest(ModelState)
        ; //end put (:

        /// <summary>
        /// deletes a <see cref="Team"/>
        /// </summary>
        /// <param name="id">the id of the team to delete</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> delete(Guid id) => await _dataAccess.deleteTeam(id) ? (IActionResult)Ok() : NotFound();
    }
}
