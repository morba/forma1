﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Forma1.DataAccess;
using Forma1.Models;
using Microsoft.AspNetCore.Authorization;

namespace Forma1.MVC.Controllers
{
    [Authorize]
    public class TeamsController : Controller
    {
        ///// <summary>
        ///// database context
        ///// </summary>
        //private readonly Forma1DbContext _context;
        
        /// <summary>
        /// data access layer
        /// </summary>
        private readonly IDataAccess _dataAccess;

        public TeamsController(Forma1DbContext context, IDataAccess dataAccess)
        {
            //_context = context;
            _dataAccess = dataAccess;
        }

        // GET: Teams
        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            return View(await _dataAccess.listAllTeams());
        }


        // GET: Teams/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Teams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,founded,championshipsWon,registrationFeePaid")] Team team)
        {
            if (ModelState.IsValid)
            {
                team.id = Guid.NewGuid();
                await _dataAccess.createNewTeam(team);
                return RedirectToAction(nameof(Index));
            }
            return View(team);
        }

        // GET: Teams/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var team = await _dataAccess.getTeamById(id ?? Guid.Empty);
            if (team == null)
            {
                return NotFound();
            }
            return View(team);
        }

        // POST: Teams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("id,name,founded,championshipsWon,registrationFeePaid")] Team team)
        {
            if (id != team.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _dataAccess.modifyTeam(team);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _dataAccess.teamExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(team);
        }

        // GET: Teams/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var team = await _dataAccess.getTeamById(id ?? Guid.Empty);
            if (team == null)
            {
                return NotFound();
            }

            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            await _dataAccess.deleteTeam(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
