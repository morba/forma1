﻿using Forma1.Models;
using Microsoft.EntityFrameworkCore;


namespace Forma1.DataAccess
{
    public class Forma1DbContext : DbContext
    {
        protected Forma1DbContext()
        {
        }

        public Forma1DbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Team> teams { get; set; }
    }
}
