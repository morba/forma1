﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Forma1.Models;

namespace Forma1.DataAccess
{
    public interface IDataAccess
    {
        Task<Team> getTeamById(Guid id);
        Task<bool> teamExists(Guid id);
        Task createNewTeam(Team team);
        Task<bool> deleteTeam(Guid teamId);
        Task editTeam(Team team);
        Task<IEnumerable<Team>> listAllTeams(CancellationToken token = default(CancellationToken));
        Task<bool> modifyTeam(Team team);
    }
}