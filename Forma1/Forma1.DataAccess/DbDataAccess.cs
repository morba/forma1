﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Forma1.Models;
using Microsoft.EntityFrameworkCore;


namespace Forma1.DataAccess
{
    public class DbDataAccess : IDataAccess
    {
        private Forma1DbContext _dbContext;

        /// <summary>
        /// default ctor. use with DI
        /// </summary>
        /// <param name="dbContext"></param>
        public DbDataAccess(Forma1DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Team>> listAllTeams(CancellationToken token = default(CancellationToken))
        {
            await _dbContext.teams.LoadAsync(token);
            return _dbContext.teams;
        }

        public async Task<Team> getTeamById(Guid id) => await _dbContext.teams.SingleOrDefaultAsync(t => t.id == id);

        public async Task createNewTeam(Team team)
        {
            _dbContext.teams.Add(team);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> teamExists(Guid id) => await _dbContext.teams.AnyAsync(t => t.id == id);


        public async Task editTeam(Team team)
        {
            _dbContext.teams.Update(team);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> modifyTeam(Team team)
        {
            //entity not tracked
            if (_dbContext.ChangeTracker.Entries<Team>().SingleOrDefault(t => t.Entity == team) == null)
            {
                Team teamInDb = await _dbContext.teams.SingleOrDefaultAsync(t => t.id == team.id);
                if (teamInDb == null)
                {
                    return false; //team does not exists in the DB
                }
                teamInDb.update(team);
            }
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> deleteTeam(Guid teamId)
        {
            Team teamInDb = await _dbContext.teams.SingleOrDefaultAsync(t => t.id == teamId);
            if(teamInDb == null)
            {
                return false;
            }
            _dbContext.teams.Remove(teamInDb);
            return (await _dbContext.SaveChangesAsync()) == 1;
        }

    }

}
