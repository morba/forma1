﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forma1.Models
{
    /// <summary>
    /// should be in a helper library in a more generalized form
    /// </summary>
    public class DateInThePast : ValidationAttribute
    {
        private DateTimeKind _dateTimeKind;

        public DateInThePast(DateTimeKind dateTimeKind, Func<string> errorMessageAccessor) : base(errorMessageAccessor)
        {
            _dateTimeKind = dateTimeKind;
        }

        public DateInThePast(DateTimeKind dateTimeKind = DateTimeKind.Utc, string errorMessage = "A múltban kell lennie!") : base(errorMessage)
        {
            _dateTimeKind = dateTimeKind;
        }

        public override bool Match(object obj) => obj as DateTime? != null;

        public override bool IsValid(object value) => ((value as DateTime?) ?? DateTime.MaxValue) <= (_dateTimeKind == DateTimeKind.Local ? DateTime.Now : DateTime.UtcNow);
    }
}
