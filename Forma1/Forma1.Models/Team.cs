﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forma1.Models
{
    /// <summary>
    /// Forma-1 team
    /// </summary>
    [TeamChampionshipsAndFounded_Validation("Több a megnyert világbajnokság mint ahány éve a csapat alapítva lett")]
    public class Team
    {
        /// <summary>
        /// unique id, primary key
        /// </summary>
        [Key]
        public Guid id { get; set; }
        
        [Display(Name = "Név")]
        [Required]
        [MinLength(3)]
        public string name { get; set; }
        
        /// <summary>
        /// currently only stores the year of foundation
        /// </summary>
        [DateInThePast(DateTimeKind.Utc, "Nem lehet a jövőben alapítva")]
        [Display(Name = "Alapítva")]
        [DataType(DataType.Date)]
        public DateTime founded { get; set; }

        /// <summary>
        /// short hand for BL
        /// </summary>
        [Display(Name = "Alapítás éve")]
        public int yearOfFoundation => founded.Year;

        /// <summary>
        /// number of world championships won. 
        /// world championships are held yearly!
        /// </summary>
        [Range(minimum:0,maximum:double.MaxValue,ErrorMessage = "A megnyert világbajnokságok száma nem lehet negatív")]
        [Display(Name = "Megnyert világbajnokságok")]
        public int championshipsWon { get; set; }

        [Display(Name = "Nevezési díj fizetve?")]
        public bool registrationFeePaid { get; set; }

        /// <summary>
        /// do not use
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        public bool valid() => championshipsWon <= DateTime.UtcNow.Year - yearOfFoundation;

        public Team update(Team otherTeam)
        {
            id = otherTeam.id;
            name = otherTeam.name;
            founded = otherTeam.founded;
            championshipsWon = otherTeam.championshipsWon;
            registrationFeePaid = otherTeam.registrationFeePaid;
            return this;
        }
    }
}
