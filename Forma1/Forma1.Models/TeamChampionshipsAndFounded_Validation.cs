﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forma1.Models
{
    /// <summary>
    /// <para>
    /// actual buisiness logic. but atm i am just too lazy to wrap my class arround another layer, might do it later (:
    /// </para>
    /// <para>
    /// on second thought this could be a database constraint too! if properly formalized...
    /// </para>
    /// </summary>
    public class TeamChampionshipsAndFounded_Validation : ValidationAttribute
    {
        public TeamChampionshipsAndFounded_Validation(Func<string> errorMessageAccessor) : base(errorMessageAccessor)
        {
        }

        public TeamChampionshipsAndFounded_Validation(string errorMessage) : base(errorMessage)
        {
        }

        public override bool Match(object obj) => obj as Team != null;

        public override bool IsValid(object value) {
            Team obj = value as Team;
            if(obj == null)
            {
                return false;
            }
            if(DateTime.UtcNow.Year - obj.yearOfFoundation < obj.championshipsWon)
            {
                return false;
            }
            return true;
        }
    }
}
