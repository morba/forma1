using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forma1.React.Controllers
{
    /// <summary>
    /// endpoint to help javascript authorize api request
    /// </summary>
    [Route("api/access_token")]
    [Authorize]
    public class AccessTokenApiController : ControllerBase
    {
        /// <summary>
        /// exchange cookie from identity server hybrid authorization for a javascript usable bearer token (=access token)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<string> get() => await HttpContext.GetTokenAsync("access_token");
    }
}
