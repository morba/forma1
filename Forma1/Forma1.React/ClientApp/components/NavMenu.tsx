import * as React from 'react';
import { NavLink, Link } from 'react-router-dom';

export class NavMenu extends React.Component<{}, {}> {
    public render() {
        return <div className='main-nav'>
                <div className='navbar navbar-inverse'>
                <div className='navbar-header'>
                    <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                        <span className='sr-only'>Toggle navigation</span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                        <span className='icon-bar'></span>
                    </button>
                    <Link className='navbar-brand' to={ '/' }>Forma1.React</Link>
                </div>
                <div className='clearfix'></div>
                <div className='navbar-collapse collapse'>
                    <ul className='nav navbar-nav'>
                        <li>
                            <NavLink exact to={ '/' } activeClassName='active'>
                                <span className='glyphicon glyphicon-home'></span> Home
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={'/access_token'} activeClassName='active'>
                                <span className='glyphicon glyphicon-qrcode'></span> Access Token
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to={ '/Teams/List' } activeClassName='active'>
                                <span className='glyphicon glyphicon-th-list'></span> Csapatok list�ja
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </div>;
    }
}
