﻿//react
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
//store
import { connect } from 'react-redux';
import { ApplicationState } from '../../store';
import * as AccessTokenStore from '../../store/AccessToken';
import * as TeamListStore from '../../store/TeamsList';
//models
import Team from '../../dto/TeamDto';
import Guid from '../../dto/Guid';
import TeamRow from './TeamRow';

interface TeamsListProps extends RouteComponentProps<{}> {
    tokenState: AccessTokenStore.AccessTokenState;
    tokenActions: typeof AccessTokenStore.actionCreators;
    teamsState: TeamListStore.TeamsListState;
    teamsActions: typeof TeamListStore.actionCreators;
}

class TeamsList extends React.Component<TeamsListProps, {}>{

    loadData() {
        if (!this.props.tokenState.has_recived) {
            this.props.tokenActions.requestAccessToken();
        }
        else {
            this.props.teamsActions.requestTeamsList();
        }
    }

    // i am just guessing in the dark here... one of these should be enough
    componentWillMount() {
        this.loadData();
    }
    componentDidMount() {
        this.loadData();
    }
    componentWillReceiveProps() {
        this.loadData();
    }
    componentWillUpdate() {
        this.loadData();
    }

    public render() {
        return <div className="row">
            <h2>Csapatok listája</h2>
            <p>
                <NavLink to="Teams/Create">Új csapat létrehozása</NavLink>
            </p>
            {this.props.tokenState.isLoading || this.props.teamsState.isLoading ? "Loading" : ""}
            <div className="table-responsive">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Név</th>
                            <th>Alapítás éve</th>
                            <th>Megnyert világbajnokságok</th>
                            <th>Nevezési díj fizetve?</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.teamsState.teams.map(team =>
                            <TeamRow team={team} />
                        )}
                    </tbody>
                </table>
            </div>
        </div>
    }
}

export default connect(
    (state: ApplicationState) => ({ tokenState: state.accessToken, teamsState: state.teamsList }),
    { ...AccessTokenStore.actionCreators, ...TeamListStore.actionCreators }
)(TeamsList) as typeof TeamsList

