﻿//react
import * as React from 'react';
import { NavLink } from 'react-router-dom';
//models
import Team from '../../dto/TeamDto';
import Guid from '../../dto/Guid';


export default class TeamRow extends React.PureComponent<{ team: Team }>
{
    public render() {
        const team = this.props.team;
        return <tr>
            <td>{team.name}</td>
            <td>{team.yearOfFoundation}</td>
            <td>{team.championshipsWon}</td>
            <td><input className="check-box" type="checkbox" disabled checked={team.registrationFeePaid} /></td>
            <td><NavLink to={`Teams/Edit/${team.id.toString()}`}>Szerkesztés</NavLink> |
                <NavLink to={`Teams/Delete/${team.id.toString()}`}>Törlés</NavLink></td>
        </tr>
    }
}