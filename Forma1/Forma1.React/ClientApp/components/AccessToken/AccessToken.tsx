﻿//react
import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../../store';
//models
import Guid from '../../dto/Guid';
import { RouteComponentProps } from 'react-router';
import * as AccessTokenStore from '../../store/AccessToken';
//componennts

type AccessTokenProps =
    & AccessTokenStore.AccessTokenState
    & typeof AccessTokenStore.actionCreators
    & RouteComponentProps<{}>;

class AccessToken extends React.Component<AccessTokenProps, {}>{
    componentWillMount() {
        // This method runs when the component is first added to the page
        this.props.requestAccessToken();
    }

    public render() {
        return <div>
            {this.props.isLoading ? <span>Loading access token...</span> : this.props.children};
        </div>
    }

}