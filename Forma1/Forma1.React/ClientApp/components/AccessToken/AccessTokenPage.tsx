﻿//react
import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState } from '../../store';
//models
import Guid from '../../dto/Guid';
import { RouteComponentProps } from 'react-router';
import * as AccessTokenStore from '../../store/AccessToken';
//componennts

type AccessTokenProps =
    & AccessTokenStore.AccessTokenState
    & typeof AccessTokenStore.actionCreators
    & RouteComponentProps<{}>;

class AccessTokenPage extends React.Component<AccessTokenProps, {}>
{
    componentWillMount() {
        // This method runs when the component is first added to the page
        this.props.requestAccessToken();
    }

    componentWillReceiveProps(nextProps: AccessTokenProps) {
        // This method runs when incoming props (e.g., route params) change
        this.props.requestAccessToken();
    }

    public render() {
        return <div className="row col-xs-12">
            <h1>Access Token</h1>
            <h4>This is a helper page, to confirm proper authorization, and help test the API with Postman</h4>
            {this.props.isLoading ? <span>Loading...</span> : []}
            <b>The access token:</b>
            <p>{this.props.access_token && this.props.access_token}</p>
        </div>
    }
}

export default connect(
    (state: ApplicationState) => state.accessToken, // Selects which state properties are merged into the component's props
    AccessTokenStore.actionCreators   // Selects which action creators are merged into the component's props
)(AccessTokenPage) as typeof AccessTokenPage;