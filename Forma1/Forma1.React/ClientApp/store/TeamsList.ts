﻿import { fetch, addTask } from 'domain-task';
import { Action, Reducer, ActionCreator } from 'redux';
import { AppThunkAction } from './';
import Guid from '../dto/Guid';
import Team from '../dto/TeamDto';
import { apiBaseUrl } from '../dto/helpers';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface TeamsListState {
    isLoading: boolean;
    has_recived: boolean;
    teams: Team[];
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestTeamsListAction {
    type: 'REQUEST_TEAMS_LIST';
}

interface ReceiveTeamsListAction {
    type: 'RECEIVE_TEAMS_LIST';
    teams: Team[];
}

interface RequestFailed {
    type: 'REQUEST_FAILED';
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestTeamsListAction | ReceiveTeamsListAction | RequestFailed;


// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    //initial get call //does nothing on the second call
    requestTeamsList: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        if (!getState().accessToken.has_recived && !getState().accessToken.isLoading) {
            const bearer_token = 'Bearer ' + getState().accessToken.access_token;
            let fetchTask = fetch(`${apiBaseUrl}/api/Team`, { headers: { 'Accepts': 'application/json', 'Authorization': bearer_token } })
                .then((response: Response) => {
                    return response.json();
                })
                .then(data => {
                    dispatch({ type: 'RECEIVE_TEAMS_LIST', teams: data });
                })
                .catch(reason => {
                    dispatch({ type: 'REQUEST_FAILED' });
                    console.log(`failed to fetch access_token: ${JSON.stringify(reason)}`)
                });
            addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
            dispatch({ type: 'REQUEST_TEAMS_LIST' });
        }
    }
};


// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: TeamsListState = { teams: [], isLoading: false, has_recived: false };

export const reducer: Reducer<TeamsListState> = (state: TeamsListState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_TEAMS_LIST':
            return {
                isLoading: true,
                teams: state.teams,
                has_recived: state.has_recived
            };
        case 'RECEIVE_TEAMS_LIST':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            return {
                teams: action.teams,
                isLoading: false,
                has_recived: true
            };
        case 'REQUEST_FAILED':
            return {
                isLoading: false,
                teams: state.teams,
                has_recived: state.has_recived
            }
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    return state || unloadedState;
};