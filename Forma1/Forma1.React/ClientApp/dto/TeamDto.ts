﻿import Guid from "ClientApp/dto/Guid";

export default class Team {
    public id: Guid;
    public name: string;
    public founded: Date;
    public yearOfFoundation: number;
    public championshipsWon: number;
    public registrationFeePaid: boolean;
}