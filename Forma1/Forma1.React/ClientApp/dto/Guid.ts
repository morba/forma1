﻿export default class Guid {
    private str: string;

    constructor(str?: string) {
        this.str = str && (Guid.isThisAGuid(str) ? str : Guid.emptyStr) || Guid.emptyStr;
    }

    toString() {
        return this.str;
    }

    isEmpty() {
        return this.str === Guid.emptyStr;
    }

    public static isThisAGuid(str: string) {
        return str.match('^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$') != null;
    }

    public static readonly emptyStr = '00000000-0000-0000-0000-000000000000';

    public static readonly Empty: Guid = new Guid(Guid.emptyStr);

    public static getNewGUIDString() {
        // your favourite guid generation function could go here
        // ex: http://stackoverflow.com/a/8809472/188246
        let d = new Date().getTime();
        if (window.performance && typeof window.performance.now === "function") {
            d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    public static newGuid() { return new Guid(Guid.getNewGUIDString()); }

}