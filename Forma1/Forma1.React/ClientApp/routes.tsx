import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import Home from './components/Home';
import AccessToken from './components/AccessToken/AccessTokenPage';
import TeamsList from './components/Teams/TeamsList';

export const routes = <Layout>
    <Route exact path='/' component={Home} />
    <Route path='/access_token' component={AccessToken} />
    <Route path='/Teams/List' component={TeamsList} />
</Layout>;
