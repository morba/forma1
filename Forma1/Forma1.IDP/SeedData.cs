﻿using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Forma1.IDP;
using Forma1.IDP.Data;
using Forma1.IDP.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Forma1.IDP
{
    public class SeedData
    {
        public static void EnsureSeedData(IServiceProvider serviceProvider)
        {
            Console.WriteLine("Seeding database...");

            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                {
                    var context = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                    context.Database.Migrate();
                    EnsureSeedData(context);
                }

                {
                    var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    context.Database.Migrate();

                    var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                    var admin = userMgr.FindByNameAsync("admin").Result;
                    if (admin == null)
                    {
                        admin = new ApplicationUser
                        {
                            UserName = "admin"
                        };
                        var result = userMgr.CreateAsync(admin, "f1test2018").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(admin, new Claim[]{
                            new Claim(JwtClaimTypes.Name, "Admin Administrator"),
                            new Claim(JwtClaimTypes.GivenName, "Admin"),
                            new Claim(JwtClaimTypes.FamilyName, "Administrator"),
                            new Claim(JwtClaimTypes.Email, "admin@example.com"),
                            new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                            new Claim(JwtClaimTypes.Address, @"{ 'street_address': 'One Hacker Way', 'locality': 'Heidelberg', 'postal_code': 69118, 'country': 'Germany' }", IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                        }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                        Console.WriteLine("admin created");
                    }
                    else
                    {
                        Console.WriteLine("admin already exists");
                    }
                }
            }

            Console.WriteLine("Done seeding database.");
            Console.WriteLine();
        }

        private static void EnsureSeedData(ConfigurationDbContext context)
        {
            if (!context.Clients.Any())
            {
                Console.WriteLine("Clients being populated");
                context.Clients.AddRange(Config.GetClients().Select(c => c.ToEntity()));
                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Adding new Clients");
                var dbClients = context.Clients.ToHashSet();
                context.Clients.RemoveRange(dbClients);
                context.SaveChanges();
                context.Clients.AddRange(Config.GetClients().Select(c=>c.ToEntity()));
                context.SaveChanges();
            }

            if (!context.IdentityResources.Any())
            {
                Console.WriteLine("IdentityResources being populated");
                context.IdentityResources.AddRange(Config.GetIdentityResources().Select(idr => idr.ToEntity()));
                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Adding new IdentityResources");
                var dbIdentityResources = context.IdentityResources.ToHashSet();
                context.IdentityResources.RemoveRange(dbIdentityResources);
                context.SaveChanges();
                context.AddRange(Config.GetIdentityResources().Select(idr => idr.ToEntity()));
                context.SaveChanges();
            }

            if (!context.ApiResources.Any())
            {
                Console.WriteLine("ApiResources being populated");
                context.ApiResources.AddRange(Config.GetApiResources().Select(ar => ar.ToEntity()));
                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Adding new ApiResources");
                var dbApiResources = context.IdentityResources.ToHashSet();
                context.RemoveRange(dbApiResources);
                context.SaveChanges();
                context.AddRange(Config.GetIdentityResources().Select(ar => ar.ToEntity()));
                context.SaveChanges();
            }
        }
    }
}
