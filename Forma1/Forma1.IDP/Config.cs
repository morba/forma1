﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace Forma1.IDP
{
    public class Config
    {
        // scopes define the resources in your system
        public static IEnumerable<IdentityResource> GetIdentityResources() => 
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiResource> GetApiResources() => 
            new List<ApiResource>
            {
                new ApiResource("api_react", "API for React SPA"),
            };

        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients() =>
            // client credentials client
            new List<Client>
            {
                // OpenID Connect hybrid flow and client credentials client (MVC)
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,

                    RequireConsent = false,

                    ClientSecrets =
                    {
                        new Secret("TmSQAVEFHozn8Ms42PY9eapCEoKTiCKZ".Sha256())
                    },

                    //normally this is set with sql in the database after deployment. i just needed a fast debug cycle
#if DEBUG 
                    RedirectUris = { "http://localhost:58631/signin-oidc" },
                    PostLogoutRedirectUris = { "http://localhost:58631/signout-callback-oidc" },
#else
                    RedirectUris = { "https://mvc.forma1.morasz.hu/signin-oidc" },
                    PostLogoutRedirectUris = { "https://mvc.forma1.morasz.hu/signout-callback-oidc" },
#endif
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },
                    AllowOfflineAccess = true
                },
                // OpenID Connect hybrid flow and client credentials client (React SPA)
                new Client
                {
                    ClientId = "react",
                    ClientName = "React SPA Client",
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,

                    RequireConsent = false,

                    ClientSecrets =
                    {
                        new Secret("Fbiqp5QhLBdkRkxrii4oq9E4kLDVBPhm".Sha256())
                    },

                    //normally this is set with sql in the database after deployment. i just needed a fast debug cycle
#if DEBUG
                    RedirectUris = { "http://localhost:54322/signin-oidc" },
                    PostLogoutRedirectUris = { "http://localhost:54322/signout-callback-oidc" },
#else
                    RedirectUris = { "https://react.forma1.morasz.hu/signin-oidc" },
                    PostLogoutRedirectUris = { "https://react.forma1.morasz.hu/signout-callback-oidc" },
#endif
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_react"
                    },
                    AllowOfflineAccess = true
                }
            };
    }
}