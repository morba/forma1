﻿using Microsoft.AspNetCore.Identity;

namespace Forma1.IDP.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
    }
}
