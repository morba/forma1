﻿using Forma1.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace Forma1.PostgreSqlMigrations
{
    /// <summary>
    /// the only reason for this project's existance is to generate the Migration code. It has absolutly no other role in the system
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public class DesignTimeContextFactory : IDesignTimeDbContextFactory<Forma1DbContext>
    {
        /// <summary>
        /// this could be any valid database connection, that connects to the same type and version as the production database 
        /// seriusly just use a local test development db
        /// </summary>
        public const string connectionString = "Server=localhost;Port=5432;Database=forma1;User Id = forma1; Password=3SAxFBgdjnmAfKvGD2VD;";

        public Forma1DbContext CreateDbContext(string[] args) => 
            new Forma1DbContext((new DbContextOptionsBuilder<Forma1DbContext>()
                .UseNpgsql(connectionString, b=>b.MigrationsAssembly("Forma1.PostgreSqlMigrations") )).Options);
    }
}
